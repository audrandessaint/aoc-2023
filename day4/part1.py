result = 0

with open("input.txt", "r") as file:
    for line in file:
        start = line.find(":") + 1
        [left, right] = line[start:].split("|")

        winning_numbers = set([int(number) for number in left.split()])
        given_numbers = set([int(number) for number in right.split()])

        number_of_matches = len(winning_numbers.intersection(given_numbers))

        if number_of_matches > 0: 
            result += 2 ** (number_of_matches - 1)

print(f"result: {result}")