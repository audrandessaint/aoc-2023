from collections import deque

result = 0

duplication_list = deque()

with open("input.txt", "r") as file:
    for line in file:
        start = line.find(":") + 1
        [left, right] = line[start:].split("|")

        winning_numbers = set([int(number) for number in left.split()])
        given_numbers = set([int(number) for number in right.split()])

        number_of_matches = len(winning_numbers.intersection(given_numbers))
        number_of_copies = 1 if len(duplication_list) == 0 else duplication_list.popleft()
        
        n = min(number_of_matches, len(duplication_list))
        duplication_list = deque(current_number_of_copies + (number_of_copies if i < n else 0)
                                for i, current_number_of_copies in enumerate(duplication_list))
        for _ in range(number_of_matches - n):
            duplication_list.append(number_of_copies + 1)

        result += number_of_copies

print(f"result: {result}")