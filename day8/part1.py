with open("input.txt", "r") as file:
    directions = file.readline()[:-1]
    file.readline()

    nodes = dict()

    for line in file:
        [node, children] = line.split(" = ")
        [left, right] = children.split(", ")
        nodes[node] = (left[1:], right[:3])

result = 0
node = "AAA"

while node != "ZZZ":
    if directions[result % len(directions)] == "L":
        node = nodes[node][0]
    elif directions[result % len(directions)] == "R":
        node = nodes[node][1]

    result += 1

print(f"result: {result}")