from math import lcm
from functools import reduce

with open("input.txt", "r") as file:
    directions = file.readline()[:-1]
    file.readline()

    nodes = dict()

    for line in file:
        [node, children] = line.split(" = ")
        [left, right] = children.split(", ")
        nodes[node] = (left[1:], right[:3])

starting_nodes = [node for node in nodes if node[-1] == "A"]
steps = []

for node in starting_nodes:
    step = 0

    while node[-1] != "Z":
        if directions[step % len(directions)] == "L":
            node = nodes[node][0] 
        elif directions[step % len(directions)] == "R":
            node = nodes[node][1]
        step += 1

    steps.append(step)

result = reduce(lcm, steps)
print(f"result: {result}")