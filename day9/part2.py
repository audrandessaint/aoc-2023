from functools import reduce

def sub_sequence(sequence):
    result = []
    for i in range(len(sequence) - 1):
        result.append(sequence[i + 1] - sequence[i])
    return result

result = 0

with open("input.txt") as file:
    for line in file:
        sequence = [int(n) for n in line.split()]
        first_elements = []

        while not all(n == 0 for n in sequence):
            first_elements.append(sequence[0])
            sequence = sub_sequence(sequence)

        result += reduce(lambda x, y: -x + y, reversed(first_elements), 0)

print(f"result: {result}")