def sub_sequence(sequence):
    result = []
    for i in range(len(sequence) - 1):
        result.append(sequence[i + 1] - sequence[i])
    return result

result = 0

with open("input.txt") as file:
    for line in file:
        sequence = [int(n) for n in line.split()]
        last_elements = []

        while not all(n == 0 for n in sequence):
            last_elements.append(sequence[-1])
            sequence = sub_sequence(sequence)

        result += sum(last_elements)

print(f"result: {result}")