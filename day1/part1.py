sum = 0

def find_first_digit(line):
    for character in line:
        if character.isdigit():
            return int(character)
        
    raise ValueError("no digit was found in the line")

with open("input.txt", "r") as file:
    for line in file:
        first_digit = find_first_digit(line)
        last_digit = find_first_digit(reversed(line))

        sum += first_digit * 10 + last_digit

print(f"Answer: {sum}")