digits = ["1", "2", "3", "4", "5", "6", "7", "8", "9", 
          "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

class PatternMatcher:
    def __init__(self, paterns, reverse=False):
        self.paterns = paterns
        self.offsets = [0] * len(digits)
        self.reverse = reverse

    def reset(self):
        self.offsets = [0] * len(digits)
    
    def update(self, character):
        for i, patern in enumerate(self.paterns):
            offset = -(self.offsets[i] + 1) if self.reverse else self.offsets[i]

            if character == patern[offset]:
                self.offsets[i] += 1
            elif (character == patern[0] and not self.reverse) \
              or (character == patern[-1] and self.reverse):
                self.offsets[i] = 1
            else:
                self.offsets[i] = 0


    def first_match(self, line):
        for i in range(len(line)):
            character = line[-(i + 1)] if self.reverse else line[i]
            self.update(character)

            for j, offset in enumerate(self.offsets):
                if offset == len(self.paterns[j]):

                    return j + 1 if j < 9 else j - 8
            
        return -1


if __name__ == "__main__":
    with open("input.txt", "r") as file:
        sum = 0
        forward_digit_matcher = PatternMatcher(digits)
        backward_digit_matcher = PatternMatcher(digits, reverse=True)

        for line in file:
            forward_digit_matcher.reset()
            backward_digit_matcher.reset()

            first_digit = forward_digit_matcher.first_match(line)
            last_digit = backward_digit_matcher.first_match(line)

            print(line, first_digit, last_digit)

            sum += first_digit * 10 + last_digit 

    print(f"Answer: {sum}")
    