from collections import defaultdict

card_set = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]

def card_value(card):
    return card_set.index(card)

def cards_value(hand):
    value = 0
    for n, card in enumerate(reversed(hand)):
        value += card_value(card) * (len(card_set) ** n)
    return value

def hand_value(hand):
    hand = hand[0]

    card_types_counts = defaultdict(int) 
    for card in hand:
        card_types_counts[card] += 1
    
    match len(card_types_counts):
        case 1:
            hand_type = 6
        case 2:
            hand_type = 5 if 4 in card_types_counts.values() else 4
        case 3:
            hand_type = 3 if 3 in card_types_counts.values() else 2
        case 4:
            hand_type = 1
        case 5:
            hand_type = 0

    return hand_type * (len(card_set) ** len(hand)) + cards_value(hand)


hands = []
with open("input.txt", "r") as file:
    for line in file:
        splited_line = line.split()
        hands.append((splited_line[0], int(splited_line[1])))

hands.sort(key=hand_value)

result = 0
for n, hand in enumerate(hands):
    result += (n + 1) * hand[1]

print(f"result: {result}")