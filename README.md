# Advent of Code 2023 | Audran's Submissions

This repository contains my submissions for the 2023th Advent of Code Advent of Code (AoC) edition. Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as interview prep, company training, university coursework, practice problems, a speed contest, or to challenge each other.

If you want to know more: https://adventofcode.com/
