def range_convert(value, destination_start, source_start, range):
    if source_start <= value and value <= source_start + range:
        return value - (source_start - destination_start)
    return value 

def map_convert(map, value):
    for range in map:
        value = range_convert(value, range[0], range[1], range[2])
    return value

maps = {
    "seed-to-soil" : [],
    "soil-to-fertilizer" : [],
    "fertilizer-to-water" : [],
    "water-to-light" : [],
    "light-to-temperature" : [],
    "temperature-to-humidity" : [],
    "humidity-to-location" : []
}

with open("input.txt", "r") as file:
    for line in file:
        splited_line = line.split()

        if len(splited_line) == 0:
            continue

        if splited_line[0] == "seeds:":
            seeds = [int(seed) for seed in splited_line[1:]]
        elif not line[0].isdigit():
            current_map = splited_line[0]
        else:
            maps[current_map].append([int(value) for value in splited_line])

locations = [] 

for seed in seeds:
    soil = map_convert(maps["seed-to-soil"], seed)
    fertilizer = map_convert(maps["soil-to-fertilizer"], soil)
    water = map_convert(maps["fertilizer-to-water"], fertilizer)
    light = map_convert(maps["water-to-light"], water)
    temperature = map_convert(maps["light-to-temperature"], light)
    humidity = map_convert(maps["temperature-to-humidity"], temperature)
    location = map_convert(maps["humidity-to-location"], humidity)

    locations.append(location)

result = min(locations)
print(f"result: {result}")
