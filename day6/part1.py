# t: race duration
# d0: previous record
# d: our final distance
# v: velocity 

# d = v * (t - v)

# we win if d > d0
# the solutions of this inequality are velocities v in between v1 and v2 the zeros of P(X) = -X^2 + tX - d0

from math import sqrt, floor, ceil

races = [
    [48, 261],
    [93, 1192],
    [84, 1019],
    [66, 1063]
]

def solve(t, d0):
    delta = t * t - 4 * d0
    return (
        (t - sqrt(delta)) / 2, 
        (t + sqrt(delta)) / 2
    )

def number_of_integers(v1, v2):
    v1 = ceil(v1)
    v2 = floor(v2)
    return v2 - v1 + 1


if __name__ == "__main__":
    result = 1

    for race in races:
        (v1, v2) = solve(race[0], race[1])
        result *= number_of_integers(v1, v2) 

    print(f"result: {result}")
