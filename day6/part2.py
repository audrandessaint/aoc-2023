from part1 import solve, number_of_integers

race_duration = 48938466
race_distance = 261119210191063

(v1, v2) = solve(race_duration, race_distance)
result = number_of_integers(v1, v2)

print(f"result: {result}")
