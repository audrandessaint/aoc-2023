from functools import reduce
import operator

max_counts = {
    "red": 0,
    "green": 0,
    "blue": 0
}

result = 0

with open("input.txt", "r") as file:
    for line in file:
        max_counts = max_counts.fromkeys(max_counts, 0)

        start = line.find(":") + 1
        draws = line[start:].replace(";", ",").split(",")
        color_counts = [(int(draw.split()[0]), draw.split()[1]) for draw in draws]

        for (count, color) in color_counts:
            max_counts[color] = max(max_counts[color], int(count))

        result += reduce(operator.mul, max_counts.values(), 1)

print(f"result: {result}")