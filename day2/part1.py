max_color_counts = {
    "red" : 12,
    "green" : 13,
    "blue" : 14,
}

def check_counts(color_counts):
    for (count, color) in color_counts:
        if max_color_counts[color] < int(count):
            return False
    return True

result = 0

with open("input.txt", "r") as file:
    for line in file:
        start = line.find(":") + 1

        draws = line[start:].replace(";", ",").split(",")
        color_counts = [(draw.split()[0], draw.split()[1]) for draw in draws]

        if not check_counts(color_counts):
            continue
         
        result += int(line[:start - 1].split()[1])

print(f"result: {result}")