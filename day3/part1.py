class LineBuffer:
    def __init__(self):
        self.previous_line = None
        self.current_line = None
        self.next_line = None

    def update(self, line):
        self.previous_line = self.current_line
        self.current_line = self.next_line
        self.next_line = line

class NumberBuffer:
    def __init__(self):
        self.value = 0
        self.is_part = False
    
    def reset(self):
        self.value = 0
        self.is_part = False

    def update(self, digit, is_part):
        self.value = self.value * 10 + digit
        self.is_part = self.is_part or is_part

def is_symbol(character):
    return not character.isdigit() and character not in {".", "\n"}

def contains_symbol_around(line, i):
    if line is None:
        return False

    if (i > 0             and is_symbol(line[i - 1])) \
    or (i < len(line) - 1 and is_symbol(line[i + 1])):
        return True

def is_engine_part(buffer, i):
    return contains_symbol_around(buffer.current_line, i) \
        or contains_symbol_around(buffer.previous_line, i) \
        or contains_symbol_around(buffer.next_line, i) \
        or (buffer.previous_line is not None and is_symbol(buffer.previous_line[i])) \
        or (buffer.next_line is not None and is_symbol(buffer.next_line[i]))

def part_number_sum(buffer):
    if buffer.current_line is None:
        return 0

    result = 0
    number = NumberBuffer()

    for i, character in enumerate(buffer.current_line):
        if character.isdigit():
            number.update(int(character), is_engine_part(buffer, i))
        else:
            result += number.value if number.is_part else 0
            number.reset()

    return result

def handle_line(line):
    buffer.update(line)
    return part_number_sum(buffer)
    

if __name__ == "__main__":
    result = 0
    buffer = LineBuffer()

    with open("input.txt", "r") as file:
        for line in file:
            result += handle_line(line)
        result += handle_line(None)

    print(f"result: {result}")
