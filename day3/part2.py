from collections import defaultdict
from copy import deepcopy

class LineBuffer:
    def __init__(self):
        self.previous_line = None
        self.current_line = None
        self.next_line = None
        self.line_number = 0

    def update(self, line):
        self.previous_line = self.current_line
        self.current_line = self.next_line
        self.next_line = line
        self.line_number += 1

class NumberBuffer:
    def __init__(self):
        self.value = 0
        self.gears = set()
    
    def reset(self):
        self.value = 0
        self.gears = set()

    def update(self, digit, gears):
        self.value = self.value * 10 + digit
        self.gears = self.gears.union(gears)
 
def add_if_gear(gears, character, gear_index):
    if character == "*":
        gears.add(gear_index)

def add_gears_around(gears, line, line_number, i):
    if line is None:
        return

    index = line_number * len(line) + i

    add_if_gear(gears, line[i], index)
    if i > 0:
        add_if_gear(gears, line[i - 1], index - 1)
    if i < len(line) - 1:
        add_if_gear(gears, line[i + 1], index + 1)

def add_surrounding_gears(gears, buffer, i):
    add_gears_around(gears, buffer.previous_line, buffer.line_number - 1, i)
    add_gears_around(gears, buffer.current_line, buffer.line_number, i)
    add_gears_around(gears, buffer.next_line, buffer.line_number + 1, i)

def gear_numbers(buffer):
    if buffer.current_line is None:
        return []

    result = []
    number = NumberBuffer()

    for i, character in enumerate(buffer.current_line):
        if character.isdigit():
            gears = set()
            add_surrounding_gears(gears, buffer, i)
            number.update(int(character), gears)
        else:
            if len(number.gears) > 0:
                result.append(deepcopy(number))
            number.reset()

    return result

def handle_line(line):
    buffer.update(line)
    return gear_numbers(buffer)
    

if __name__ == "__main__":
    numbers = []
    buffer = LineBuffer()

    with open("input.txt", "r") as file:
        for line in file:
            numbers.extend(handle_line(line))
        numbers.extend(handle_line(None))

    gears = defaultdict(list) 

    for number in numbers:
        for gear in number.gears:
            gears[gear].append(number)

    result = 0

    for gear in gears:
        if len(gears[gear]) == 2:
            result += gears[gear][0].value * gears[gear][1].value

    print(f"result: {result}")
