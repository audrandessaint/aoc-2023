galaxies = []

def min_max(a, b):
    return min(a, b), max(a, b)

def solution(expansion_factor):
    with open("input.txt", "r") as file:
        for line_number, line in enumerate(file):
            for line_offset, space in enumerate(line):
                if space == "#":
                    galaxies.append((line_number, line_offset)) 

    empty_rows = [i for i in range(line_offset + 1) if i not in [galaxy[0] for galaxy in galaxies]]
    empty_columns = [j for j in range(line_number + 1) if j not in [galaxy[1] for galaxy in galaxies]]
 
    result = 0

    for i, galaxy1 in enumerate(galaxies):
        for galaxy2 in galaxies[:i]:
            min_row, max_row = min_max(galaxy1[0], galaxy2[0])
            min_column, max_column = min_max(galaxy1[1], galaxy2[1])
            result += (max_row - min_row)       + (expansion_factor - 1) * len([i for i in empty_rows if min_row < i < max_row])
            result += (max_column - min_column) + (expansion_factor - 1) * len([j for j in empty_columns if min_column < j < max_column]) 

    return result

if __name__ == "__main__":
    print(f"result: {solution(2)}")